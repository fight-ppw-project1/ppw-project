from django.urls import path
from .views import index, dynamic_index, donate_form, submit_donation

urlpatterns = [
    path('', index, name='donatelanding'),
    path('donationForm', donate_form, name="donate_form"),
    path('<int:event_id>', dynamic_index, name='dynamicIndex'),
    path('submit_donation', submit_donation, name='submitDonate')
]
