from django.http import HttpResponseRedirect
from django.shortcuts import render

from donationPage.forms import FormDonation
from donationPage.models import Donation, Donateurs


# Create your views here.
# @property
# def progress(self):
#     if Math.abs((self.current)*100/self.target) >= 100:
#         return 100
#     else:
#         return Math.abs((self.current)*100/self.target)


def donate_form(request):
    response = {'form': FormDonation, 'availableProg': Donation.objects.all()}
    return render(request, 'donationPage/donateForm.html', response)


def submit_donation(request):
    if request.method == "POST":
        temp = FormDonation(request.POST)
        if temp.is_valid():
            valid = temp.cleaned_data
            entry = Donateurs(name=valid['name'], email=valid['email'], amount=valid['amount'],
                              program=valid['program'])
            entry2 = Donation.objects.get(id=valid['program'].id)
            entry2.donate(valid['amount'])
            entry2.save()
            entry.save()
            return HttpResponseRedirect('/')
        else:
            return render(request, 'donationPage/_donationLanding.html')


def index(request):
    donations = {'programs': Donation.objects.all()}
    return render(request, 'donationPage/_donationLanding.html', donations)


def dynamic_index(request, event_id):
    response = {'event': Donation.objects.get(id=event_id), 'donateurs': Donateurs.objects.order_by('-created')[:3]}
    return render(request, 'donationPage/_donationpage.html', response)
