from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
from datetime import datetime


# Create your models here.
class Donation(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    date = models.DateField()
    shortdescription = models.TextField(max_length=200)
    description = models.TextField()
    target = models.IntegerField()
    current = models.IntegerField()
    image = models.ImageField(upload_to='static/media/model/Donation/')
    created = models.DateTimeField(auto_now_add=True)

    def donate(self, amount):
        if self.current >= self.target:
            return
        self.current += amount

    def __str__(self):
        return '{p.name}'.format(p=self)


class Donateurs(models.Model):
    program = models.ForeignKey(Donation, on_delete=models.CASCADE)
    name = models.CharField(max_length=300, default='Anonymous')
    email = models.CharField(max_length=30, default="youremai@random.com")
    amount = models.PositiveIntegerField(validators=[MinValueValidator(1)])
    hidden = models.BooleanField(default=False)
    created = models.DateTimeField(default=datetime.now())

    def __str__(self):
        return '{p.program} {p.name}'.format(p=self)
