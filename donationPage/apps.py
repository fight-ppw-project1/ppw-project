from django.apps import AppConfig


class DonationpageConfig(AppConfig):
    name = 'donationPage'
