from django.test import TestCase, Client
from django.urls import resolve

from .models import Donation
from .views import index, donate_form


# Create your tests here.
class DonateTestURLAndRender(TestCase):
    def setUp(self):
        data = {
            'name': 'testprog',
            'date': '2018-12-20',
            'shortdescription': 'short',
            'description': 'long',
            'target': 10000,
            'current': 0,
            'image': 'https://www.helpmeplsimtiredasf.com',
        }
        donation = Donation(**data)
        donation.save()

    def test_donationPage_url_doesnt_exist(self):
        response = Client().get('/pop')
        self.assertEqual(response.status_code, 404)

    def test_donation_url_does_exist(self):
        response = Client().get('/program/')
        self.assertEqual(response.status_code, 200)

    def test_donation_program_url_exists(self):
        response = Client().get('/program/1')
        self.assertEqual(response.status_code, 200)

    def test_donation_browse_view_render_correctly(self):
        donation = Donation.objects.get(name='testprog')
        response = Client().get('/program/')
        self.assertTemplateUsed(response, 'donationPage/_donationLanding.html')
        self.assertContains(response, donation.name)
        self.assertContains(response, donation.shortdescription)

    def test_donation_page_view_render_correctly(self):
        donation = Donation.objects.get(name='testprog')
        response = Client().get('/program/1')
        self.assertTemplateUsed(response, 'donationPage/_donationpage.html')
        self.assertContains(response, donation.name)
        self.assertContains(response, donation.shortdescription)
        self.assertContains(response, donation.description)
        self.assertContains(response, donation.current)
        self.assertContains(response, donation.image)


# class DonateursTest(TestCase):
#     def setUp(self):
#         data = {
#             'name': 'testprog',
#             'date': '2018-12-20',
#             'shortdescription': 'short',
#             'description': 'long',
#             'target': 10000,
#             'current': 0,
#             'image': 'https://www.helpmeplsimtiredasf.com',
#         }
#         donation = Donation(**data)
#         donation.save()
#
#     def test_donation_form_success(self):
#         donation = Donation.objects.get(name='testprog')
#         data = {
#             'program': donation.id,
#             'name': 'andre',
#             'email': 'dre@email.com',
#             'amount': 50000,
#             'hidden': True,
#         }
#
#         response = Client().post(reverse('/donateForm/1', data=data))
#
#         donation_count = Donation.objects.all().count()
#         self.assertEqual(donation_count, 1)
#
#     def test_donation_form_failed(self):
#         donation = Donation.objects.get(name='testprog')
#         data = {
#             'program': None,
#             'name': '',
#             'email': '',
#             'amount': None,
#             'hidden': True,
#         }
#
#         response = Client().post(reverse('/donateForm/1', data=data))
#
#         donation_count = Donation.objects.all().count()
#         self.assertEqual(donation_count, 1)


class TestFunc(TestCase):
    def test_index_used(self):
        response = resolve('/program/')
        self.assertEqual(response.func, index)

    def test_donate_form_used(self):
        self.assertEqual(resolve('/program/donationForm').func, donate_form)


class DonasiModelsTest(TestCase):
    def test_donation_saved(self):
        data = {
            'name': 'test',
            'date': '2018-12-20',
            'shortdescription': 'short',
            'description': 'long',
            'target': 10000,
            'current': 0,
            'image': 'https://www.helpmeplsimtiredasf.com',
        }
        program_donasi = Donation(**data)
        program_donasi.save()

        query = Donation.objects.filter(**data)
        self.assertEqual(1, len(query))
        self.assertEqual(program_donasi, query[0])
        self.assertEqual(program_donasi.name, "test")
