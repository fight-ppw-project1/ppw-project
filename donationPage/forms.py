from django import forms

from donationPage.models import Donation


class FormDonation(forms.Form):
    nameAttr = {
        'id': 'name',
        'class': 'form-control',
        'placeholder':'Name',
        'type': 'text'
    }
    emailAttr = {
        'id': 'email',
        'class': 'form-control',
        'placeholder':'Email',
        'type': 'email'
    }
    amountAttr = {
        'id': 'amount',
        'class': 'form-control',
        'placeholder':'Amount',
        'type': 'number'
    }
    anonAttr = {
        'id': 'anonCheck',
        'class': 'form-control',
        'placeholder':'Anonymous',
        'type':'checkbox',

    }
    programAttr = {
        'id': 'program',
        'placeholder':'Programs',
        'class': 'form-control',
        'type': 'select'
    }
    name = forms.CharField(max_length=50, label="", required=True, widget=forms.TextInput(attrs=nameAttr),initial="Anonymous")
    email = forms.EmailField(max_length=250, label="", required=True, widget=forms.EmailInput(attrs=emailAttr))
    amount = forms.IntegerField(label="", required=True, widget=forms.NumberInput(attrs=amountAttr))
    program = forms.ModelChoiceField(label="", required=True, queryset=Donation.objects.all().order_by('name'),
                                     widget=forms.Select(attrs=programAttr), empty_label="Select Programs",
                                     to_field_name='name')
    anon = forms.BooleanField(label="Hidden", widget=forms.CheckboxInput(attrs=anonAttr),required=False,initial=False)
