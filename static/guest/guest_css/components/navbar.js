$(function () {
  $(document).scroll(function () {
    var $nav = $(".navbar");
    var $topper =$('.entryContainer')
    if($(this).scrollTop() > $topper.height()){
      $nav.removeClass('navbar-dark');
      $nav.removeClass('bg-transparent');
      $nav.addClass('navbar-light');
      $nav.addClass('bg-light');
    }
    else{
      $nav.removeClass('navbar-light');
      $nav.addClass('navbar-dark');
      $nav.addClass('bg-transparent');
    }
  });
});
