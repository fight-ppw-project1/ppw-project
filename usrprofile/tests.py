from django.test import TestCase, Client
from django.urls import resolve
from .views import *


# Create your tests here.
class ProfileTest(TestCase):
    def test_if_profile_available(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_if_profile_using_template(self):
        self.assertTemplateUsed('user.html')

    def test_if_using_index_function(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    # def test_if_css_is_used(self):
    #     test = Client().get('/profile')
    #     self.assertEqual(test,  )
