from django.shortcuts import render
from donationPage.models import *
# Create your views here.

def index(request):
    attrs ={
        'donation':Donation.objects.all()
    }
    html = 'usrprofile/user.html'
    return render(request,html,attrs)