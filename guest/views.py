from django.shortcuts import render
from donationPage.models import Donation


# Create your views here.
def index(request):
    donations = {'programs': Donation.objects.all().first(), 'others': Donation.objects.all()[1:3]}
    return render(request, 'guest/containers/landingPage.html', donations)
