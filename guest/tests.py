from django.test import TestCase, Client
from django.urls import resolve

from donationPage.models import Donation
from guest.views import index


# Create your tests here.
class GuestTest(TestCase):
    def setUp(self):
        args1 = {
            'name': 'testprog',
            'date': '2018-12-20',
            'shortdescription': 'short',
            'description': 'long',
            'target': 10000,
            'current': 0,
            'image': 'https://www.helpmeplsimtiredasf.com',
        }
        args2 = {
            'name': 'testprog2',
            'date': '2018-12-20',
            'shortdescription': 'short2',
            'description': 'long2',
            'target': 100000,
            'current': 0,
            'image': 'https://www.helpmeplsimtiredasf.com',
        }
        args3 = {
            'name': 'testprog3',
            'date': '2018-12-20',
            'shortdescription': 'short3',
            'description': 'long3',
            'target': 1000000,
            'current': 0,
            'image': 'https://www.helpmeplsimtiredasf.com',
        }
        args4 = {
            'name': 'testprog4',
            'date': '2018-12-20',
            'shortdescription': 'short4',
            'description': 'long4',
            'target': 1000000,
            'current': 0,
            'image': 'https://www.helpmeplsimtiredasf.com',
        }
        Donation.objects.create(**args1)
        Donation.objects.create(**args2)
        Donation.objects.create(**args3)
        Donation.objects.create(**args4)

    def testUsingIndexFunction(self):
        self.assertEqual(resolve('/').func, index)

    def testIndexUrlExist(self):
        self.assertEqual(Client().get('/').status_code, 200)

    def testUsingIndexHtml(self):
        self.assertTemplateUsed(Client().get('/'), 'guest/containers/landingPage.html')
