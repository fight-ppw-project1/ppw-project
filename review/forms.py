from django import forms


class UserReview(forms.Form):
    usernameAttr = {
        'id': 'username',
        'class': 'form-control',
        'placeholder': 'Name',
        'type': 'text',
        'readonly': 'readonly'

    }
    reviewAttr = {
        'id': 'userreview',
        'class': 'form-control',
        'placeholder': 'Review',
        'type': 'text',
        'cols': 10,
        'rows': 5,
    }
    username = forms.CharField(
        label="", required=True, widget=forms.TextInput(attrs=usernameAttr))
    usrReview = forms.CharField(
        label="", required=True, max_length=200, widget=forms.Textarea(attrs=reviewAttr))
