from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Review(models.Model):  # pragma: no cover
    name = models.CharField(max_length=100)
    reviewEntry = models.TextField(max_length=300)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):  # pragma: no cover
        return '{p.name}'.format(p=self)
