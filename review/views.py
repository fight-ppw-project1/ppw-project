from django.http import JsonResponse
from django.shortcuts import render

from .forms import UserReview
from .models import Review


# Create your views here.


def index(request):
    entries = Review.objects.order_by('-created')[0:3]
    initial = {'username': request.user.username}
    form = UserReview(initial=initial)
    return render(request, 'Review/_reviewcontent.html', {'form': form, 'entries': entries, })


def postFunc(request):
    if request.method == "POST" and request.is_ajax():
        form = UserReview(request.POST)
        if form.is_valid():
            name = form.cleaned_data['username']
            reviewEntry = form.cleaned_data['usrReview']
            Review.objects.create(
                name=name,
                reviewEntry=reviewEntry,
            ).save()
    return JsonResponse({'data': list(Review.objects.all().values())})
