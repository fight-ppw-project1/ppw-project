from django.test import TestCase, Client
from django.urls import resolve

from review.views import index


# Create your tests here.


class TestReview(TestCase):
    def testUsingReviewFunction(self):
        self.assertEqual(resolve('/review/').func, index)

    def testReviewUrlExist(self):
        self.assertEqual(Client().get('/review/').status_code, 200)

    def testReviewUseHTML(self):
        self.assertTemplateUsed(Client().get('/review/'), 'Review/review.html')
