from django.urls import path
from review.views import index, postFunc

urlpatterns = [
    path('', index, name='reviewPage'),
    path('post', postFunc, name="postReview"),
]
