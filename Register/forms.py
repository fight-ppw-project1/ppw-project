from django import forms


class UserRegister(forms.Form):
    nameAttr = {
        'id': 'name',
        'class': 'form-control',
        'placeholder': 'Name',
        'type': 'text'
    }
    usernameAttr = {
        'id': 'username',
        'class': 'form-control',
        'placeholder': 'Username',
        'type': 'text'
    }
    emailAttr = {
        'id': 'email',
        'class': 'form-control',
        'placeholder': 'Email',
        'type': 'email'
    }
    passwordAttr = {
        'id': 'password',
        'class': 'form-control',
        'placeholder': 'Password',
        'type': 'password'
    }
    confirmPasswordAttr = {
        'id': 'confirmPassword',
        'class': 'form-control',
        'placeholder': 'Confirm Password',
        'type': 'password'
    }
    name = forms.CharField(label="", required=True, max_length=50,
                           widget=forms.TextInput(attrs=nameAttr))
    username = forms.CharField(label="", required=True, max_length=20,
                               widget=forms.TextInput(attrs=usernameAttr))
    email = forms.EmailField(label="", required=True, widget=forms.EmailInput(attrs=emailAttr))
    password = forms.CharField(label="", required=True, max_length=20,
                               widget=forms.PasswordInput(attrs=passwordAttr))
    confirmPassword = forms.CharField(label="", required=True, max_length=20,
                                      widget=forms.PasswordInput(attrs=confirmPasswordAttr))


class LoginUser(forms.Form):
    usernameAttr = {
        'id': 'username',
        'class': 'form-control',
        'placeholder': 'Username',
        'type': 'text',
    }
    passwordAttr = {
        'id': 'password',
        'class': 'form-control',
        'placeholder': 'Password',
        'type': 'password'
    }
    username = forms.CharField(label="", required=True, widget=forms.TextInput(attrs=usernameAttr))
    password = forms.CharField(label="", required=True, max_length=20,
                               widget=forms.PasswordInput(attrs=passwordAttr))
