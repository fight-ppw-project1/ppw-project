from django.urls import path
from Register.views import register, add_user, my_login, my_logout,signin

urlpatterns = [
    path('', register, name='register'),
    path('signin', signin, name='signin'),
    path('submitUser/', add_user, name='submit_user'),
    path('login', my_login, name='login'),
    path('logout', my_logout, name='logout')
]
