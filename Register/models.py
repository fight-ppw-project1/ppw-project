from django.contrib.auth.models import User
from django.db import models
from donationPage.models import Donation


# Create your models here.
class Donator(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
