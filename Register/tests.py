from django.contrib.auth import get_user
from django.test import TestCase, Client
from django.urls import resolve

from Register.views import register


# Create your tests here.
class TestRegister(TestCase):
    def testUsingRegisterFunction(self):
        self.assertEqual(resolve('/register/').func, register)

    def testRegisterUrlExist(self):
        self.assertEqual(Client().get('/register/').status_code, 200)

    def testUsingRegisterHtml(self):
        self.assertTemplateUsed(Client().get('/register/'), 'Register/register.html')


class TestUser(TestCase):
    def setUp(self):
        self.client.post('/register/submitUser',
                         {'name': 'testName', 'username': 'testUName', 'email': 'test@mail.com', 'password': 'testPass',
                          'confirmPassword': 'testPass'})

    # def test_create_user(self):
    #     self.assertEqual(Donator.objects.all().count(), 1)
    #
    # def test_login(self):
    #     self.client.post('/register/login', {'username': 'testUName', 'password': 'testPass'})
    #     self.assertTrue(get_user(self.client).is_authenticated)

    def test_logout(self):
        self.client.post('/register/login', {'username': 'testUName', 'password': 'testPass'})
        self.client.get('/register/logout')
        self.assertFalse(get_user(self.client).is_authenticated)
