from django.contrib import messages
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render
from Register.forms import UserRegister, LoginUser
from Register.models import Donator


# Create your views here.
def register(request):
    response = {'form': UserRegister, 'login': LoginUser}
    return render(request, 'Register/registrationForm.html', response)

def signin(request):
    response = {'login': LoginUser}
    return render(request, 'Register/loginPage.html', response)


def add_user(request):
    if request.method == "POST":
        temp = UserRegister(request.POST)
        if temp.is_valid():
            valid = temp.cleaned_data
            if valid['confirmPassword'] == valid['password']:
                user = User.objects.create_user(email=valid['email'], password=valid['password'],
                                                username=valid['username'])
                entry = Donator(user=user, name=valid['name'])
                entry.save()
                return HttpResponseRedirect('/')
            else:
                messages.error(request, 'Password didnt match')
                return HttpResponseRedirect('/register/')
        else:
            return HttpResponseRedirect('/register/')
    else:
        return HttpResponseRedirect('/register/')


def my_login(request):
    if not request.user.is_authenticated:
        if request.method == "POST":
            form = LoginUser(request.POST)
            if form.is_valid():
                donator = authenticate(request, username=form.cleaned_data['username'],
                                       password=form.cleaned_data['password'])
                if donator is not None:
                    login(request, donator)
                    return HttpResponseRedirect('/')
                else:
                    messages.error(request, 'WRONG')
                    return HttpResponseRedirect('/')
            else:
                messages.error(request, 'not valid')
                return HttpResponseRedirect('/')
        else:
            return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')


@login_required
def my_logout(request):
    if request.user.is_authenticated:
        logout(request)
    return HttpResponseRedirect('/')
