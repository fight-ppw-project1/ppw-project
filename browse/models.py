from django.db import models


# Create your models here.
class News(models.Model):
    id = models.AutoField(primary_key=True)
    image = models.ImageField(upload_to='static/media/model/News')
    event = models.CharField(max_length=200)
    date = models.DateField()
    desc = models.TextField(max_length=300)
    link = models.CharField(max_length=1000, default="")
    categories = models.CharField(max_length=100, default="")
