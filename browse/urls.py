from django.urls import path
from browse.views import index

urlpatterns = [
    path('', index, name='newspage')
]
