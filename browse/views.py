from django.shortcuts import render
from browse.models import News


# Create your views here.
def index(request):
    response = {'news': News.objects.all()}
    return render(request, 'browse/news.html', response)
