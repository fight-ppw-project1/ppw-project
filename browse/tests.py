from django.urls import resolve
from django.test import TestCase, Client
from .views import index


# test case here
class BrowseTest(TestCase):
    def test_browse_has_url_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code, 200)

    def test_browse_url_using_template(self):
        response = Client().get('/news/')
        self.assertTemplateUsed(response, 'browse/news.html')

    def test_browse_is_using_index(self):
        found = resolve('/news/')
        self.assertEqual(found.func, index)
