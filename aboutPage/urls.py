from django.urls import path
from aboutPage.views import index

urlpatterns = [
    path('', index, name='about')
]
